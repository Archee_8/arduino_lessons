// keyboard publisher
#include <termios.h>
#include <iostream>
#include <stdio.h>
#include <ros/ros.h>
#include "std_msgs/Char.h"


int main(int argc, char **argv)
{
    
    ros::init(argc, argv, "arduino_keyboard");
    ros::NodeHandle n;

    ros::Publisher pub = n.advertise<std_msgs::Char>("Key", 1000);
    ros::Rate loop_rate(10);


    while (ros::ok())
    {
    
        std_msgs::Char c;
        c.data = getchar();
        pub.publish(c);
        ros::spinOnce();
        loop_rate.sleep();
    }
}