#include <LiquidCrystal.h>
#include <ros.h>
#include <std_msgs/Char.h>

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

ros::NodeHandle  nh;
byte point[8] = {
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
};
int col = 8;
int row = 1;
void messageCb( const std_msgs::Char& msg) {
char c = msg.data;
  switch (c) {
    case 'w':
      row = row - 1;
      if (row == 2) {
        row = 0;
      }
      if (row == -1) {
        row = 1;
      }
      printDisplay(col, row);
      break;
    case 's':
      row = row + 1;
      if (row == 2) {
        row = 0;
      }
      if (row == -1) {
        row = 1;
      }
      printDisplay(col, row);
      break;
    case 'a':
      col = col - 1;
      if (col == 16) {
        col = 0;
      }
      if (col == -1) {
        col = 15;
      }
      printDisplay(col, row);
      break;
    case 'd':
      col = col + 1;
      if (col == 16) {
        col = 0;
      }
      if (col == -1) {
        col = 15;
      }
      printDisplay(col, row);
      break;

    default:
      break;
  }
}

void printDisplay(int col, int row) {

lcd.clear();
  lcd.setCursor(col, row);
  lcd.write(byte(0));
  
  
}
ros::Subscriber<std_msgs::Char> sub("Key", &messageCb );


void setup() {
  lcd.createChar(0, point);
  lcd.begin(16, 2);
  lcd.setCursor(col, row);
  lcd.write(byte(0));
  nh.initNode();
  nh.subscribe(sub);



}
void loop() {
  
  nh.spinOnce();
  delay(1);

}
