#ifndef _ROS_SERVICE_lookupTransform_h
#define _ROS_SERVICE_lookupTransform_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "geometry_msgs/TransformStamped.h"
#include "ros/time.h"

namespace tf_lookup
{

static const char LOOKUPTRANSFORM[] = "tf_lookup/lookupTransform";

  class lookupTransformRequest : public ros::Msg
  {
    public:
      typedef const char* _target_frame_type;
      _target_frame_type target_frame;
      typedef const char* _source_frame_type;
      _source_frame_type source_frame;
      typedef ros::Time _transform_time_type;
      _transform_time_type transform_time;

    lookupTransformRequest():
      target_frame(""),
      source_frame(""),
      transform_time()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      uint32_t length_target_frame = strlen(this->target_frame);
      varToArr(outbuffer + offset, length_target_frame);
      offset += 4;
      memcpy(outbuffer + offset, this->target_frame, length_target_frame);
      offset += length_target_frame;
      uint32_t length_source_frame = strlen(this->source_frame);
      varToArr(outbuffer + offset, length_source_frame);
      offset += 4;
      memcpy(outbuffer + offset, this->source_frame, length_source_frame);
      offset += length_source_frame;
      *(outbuffer + offset + 0) = (this->transform_time.sec >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->transform_time.sec >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->transform_time.sec >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->transform_time.sec >> (8 * 3)) & 0xFF;
      offset += sizeof(this->transform_time.sec);
      *(outbuffer + offset + 0) = (this->transform_time.nsec >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->transform_time.nsec >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->transform_time.nsec >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->transform_time.nsec >> (8 * 3)) & 0xFF;
      offset += sizeof(this->transform_time.nsec);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      uint32_t length_target_frame;
      arrToVar(length_target_frame, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_target_frame; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_target_frame-1]=0;
      this->target_frame = (char *)(inbuffer + offset-1);
      offset += length_target_frame;
      uint32_t length_source_frame;
      arrToVar(length_source_frame, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_source_frame; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_source_frame-1]=0;
      this->source_frame = (char *)(inbuffer + offset-1);
      offset += length_source_frame;
      this->transform_time.sec =  ((uint32_t) (*(inbuffer + offset)));
      this->transform_time.sec |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->transform_time.sec |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      this->transform_time.sec |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      offset += sizeof(this->transform_time.sec);
      this->transform_time.nsec =  ((uint32_t) (*(inbuffer + offset)));
      this->transform_time.nsec |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->transform_time.nsec |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      this->transform_time.nsec |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      offset += sizeof(this->transform_time.nsec);
     return offset;
    }

    const char * getType(){ return LOOKUPTRANSFORM; };
    const char * getMD5(){ return "bb9d983758e61f286b43546ac9c0b080"; };

  };

  class lookupTransformResponse : public ros::Msg
  {
    public:
      typedef geometry_msgs::TransformStamped _transform_type;
      _transform_type transform;

    lookupTransformResponse():
      transform()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->transform.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->transform.deserialize(inbuffer + offset);
     return offset;
    }

    const char * getType(){ return LOOKUPTRANSFORM; };
    const char * getMD5(){ return "627ebb4e09bbb127f87308bbfdbaec08"; };

  };

  class lookupTransform {
    public:
    typedef lookupTransformRequest Request;
    typedef lookupTransformResponse Response;
  };

}
#endif
