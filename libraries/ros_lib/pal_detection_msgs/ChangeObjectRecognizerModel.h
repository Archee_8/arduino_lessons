#ifndef _ROS_SERVICE_ChangeObjectRecognizerModel_h
#define _ROS_SERVICE_ChangeObjectRecognizerModel_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace pal_detection_msgs
{

static const char CHANGEOBJECTRECOGNIZERMODEL[] = "pal_detection_msgs/ChangeObjectRecognizerModel";

  class ChangeObjectRecognizerModelRequest : public ros::Msg
  {
    public:
      typedef const char* _model_name_type;
      _model_name_type model_name;
      typedef bool _reset_desired_classes_param_type;
      _reset_desired_classes_param_type reset_desired_classes_param;

    ChangeObjectRecognizerModelRequest():
      model_name(""),
      reset_desired_classes_param(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      uint32_t length_model_name = strlen(this->model_name);
      varToArr(outbuffer + offset, length_model_name);
      offset += 4;
      memcpy(outbuffer + offset, this->model_name, length_model_name);
      offset += length_model_name;
      union {
        bool real;
        uint8_t base;
      } u_reset_desired_classes_param;
      u_reset_desired_classes_param.real = this->reset_desired_classes_param;
      *(outbuffer + offset + 0) = (u_reset_desired_classes_param.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->reset_desired_classes_param);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      uint32_t length_model_name;
      arrToVar(length_model_name, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_model_name; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_model_name-1]=0;
      this->model_name = (char *)(inbuffer + offset-1);
      offset += length_model_name;
      union {
        bool real;
        uint8_t base;
      } u_reset_desired_classes_param;
      u_reset_desired_classes_param.base = 0;
      u_reset_desired_classes_param.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->reset_desired_classes_param = u_reset_desired_classes_param.real;
      offset += sizeof(this->reset_desired_classes_param);
     return offset;
    }

    const char * getType(){ return CHANGEOBJECTRECOGNIZERMODEL; };
    const char * getMD5(){ return "c46978b1fa0abe1a947f524717b449fd"; };

  };

  class ChangeObjectRecognizerModelResponse : public ros::Msg
  {
    public:
      typedef bool _status_type;
      _status_type status;

    ChangeObjectRecognizerModelResponse():
      status(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_status;
      u_status.real = this->status;
      *(outbuffer + offset + 0) = (u_status.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->status);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_status;
      u_status.base = 0;
      u_status.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->status = u_status.real;
      offset += sizeof(this->status);
     return offset;
    }

    const char * getType(){ return CHANGEOBJECTRECOGNIZERMODEL; };
    const char * getMD5(){ return "3a1255d4d998bd4d6585c64639b5ee9a"; };

  };

  class ChangeObjectRecognizerModel {
    public:
    typedef ChangeObjectRecognizerModelRequest Request;
    typedef ChangeObjectRecognizerModelResponse Response;
  };

}
#endif
