#ifndef _ROS_pal_detection_msgs_RecognizedObject_h
#define _ROS_pal_detection_msgs_RecognizedObject_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "sensor_msgs/RegionOfInterest.h"

namespace pal_detection_msgs
{

  class RecognizedObject : public ros::Msg
  {
    public:
      typedef const char* _object_class_type;
      _object_class_type object_class;
      typedef float _confidence_type;
      _confidence_type confidence;
      typedef sensor_msgs::RegionOfInterest _bounding_box_type;
      _bounding_box_type bounding_box;

    RecognizedObject():
      object_class(""),
      confidence(0),
      bounding_box()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      uint32_t length_object_class = strlen(this->object_class);
      varToArr(outbuffer + offset, length_object_class);
      offset += 4;
      memcpy(outbuffer + offset, this->object_class, length_object_class);
      offset += length_object_class;
      union {
        float real;
        uint32_t base;
      } u_confidence;
      u_confidence.real = this->confidence;
      *(outbuffer + offset + 0) = (u_confidence.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_confidence.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_confidence.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_confidence.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->confidence);
      offset += this->bounding_box.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      uint32_t length_object_class;
      arrToVar(length_object_class, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_object_class; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_object_class-1]=0;
      this->object_class = (char *)(inbuffer + offset-1);
      offset += length_object_class;
      union {
        float real;
        uint32_t base;
      } u_confidence;
      u_confidence.base = 0;
      u_confidence.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_confidence.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_confidence.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_confidence.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->confidence = u_confidence.real;
      offset += sizeof(this->confidence);
      offset += this->bounding_box.deserialize(inbuffer + offset);
     return offset;
    }

    const char * getType(){ return "pal_detection_msgs/RecognizedObject"; };
    const char * getMD5(){ return "ac3fbc481abe751cad38199e3707858d"; };

  };

}
#endif