#ifndef _ROS_SERVICE_RenameMap_h
#define _ROS_SERVICE_RenameMap_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace pal_navigation_msgs
{

static const char RENAMEMAP[] = "pal_navigation_msgs/RenameMap";

  class RenameMapRequest : public ros::Msg
  {
    public:
      typedef const char* _current_map_name_type;
      _current_map_name_type current_map_name;
      typedef const char* _new_map_name_type;
      _new_map_name_type new_map_name;

    RenameMapRequest():
      current_map_name(""),
      new_map_name("")
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      uint32_t length_current_map_name = strlen(this->current_map_name);
      varToArr(outbuffer + offset, length_current_map_name);
      offset += 4;
      memcpy(outbuffer + offset, this->current_map_name, length_current_map_name);
      offset += length_current_map_name;
      uint32_t length_new_map_name = strlen(this->new_map_name);
      varToArr(outbuffer + offset, length_new_map_name);
      offset += 4;
      memcpy(outbuffer + offset, this->new_map_name, length_new_map_name);
      offset += length_new_map_name;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      uint32_t length_current_map_name;
      arrToVar(length_current_map_name, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_current_map_name; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_current_map_name-1]=0;
      this->current_map_name = (char *)(inbuffer + offset-1);
      offset += length_current_map_name;
      uint32_t length_new_map_name;
      arrToVar(length_new_map_name, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_new_map_name; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_new_map_name-1]=0;
      this->new_map_name = (char *)(inbuffer + offset-1);
      offset += length_new_map_name;
     return offset;
    }

    const char * getType(){ return RENAMEMAP; };
    const char * getMD5(){ return "18e17ff8673092bf2b0ad3d839b9943d"; };

  };

  class RenameMapResponse : public ros::Msg
  {
    public:
      typedef bool _success_type;
      _success_type success;
      typedef const char* _message_type;
      _message_type message;

    RenameMapResponse():
      success(0),
      message("")
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.real = this->success;
      *(outbuffer + offset + 0) = (u_success.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->success);
      uint32_t length_message = strlen(this->message);
      varToArr(outbuffer + offset, length_message);
      offset += 4;
      memcpy(outbuffer + offset, this->message, length_message);
      offset += length_message;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.base = 0;
      u_success.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->success = u_success.real;
      offset += sizeof(this->success);
      uint32_t length_message;
      arrToVar(length_message, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_message; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_message-1]=0;
      this->message = (char *)(inbuffer + offset-1);
      offset += length_message;
     return offset;
    }

    const char * getType(){ return RENAMEMAP; };
    const char * getMD5(){ return "937c9679a518e3a18d831e57125ea522"; };

  };

  class RenameMap {
    public:
    typedef RenameMapRequest Request;
    typedef RenameMapResponse Response;
  };

}
#endif
