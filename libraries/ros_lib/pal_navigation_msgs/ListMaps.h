#ifndef _ROS_SERVICE_ListMaps_h
#define _ROS_SERVICE_ListMaps_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace pal_navigation_msgs
{

static const char LISTMAPS[] = "pal_navigation_msgs/ListMaps";

  class ListMapsRequest : public ros::Msg
  {
    public:

    ListMapsRequest()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
     return offset;
    }

    const char * getType(){ return LISTMAPS; };
    const char * getMD5(){ return "d41d8cd98f00b204e9800998ecf8427e"; };

  };

  class ListMapsResponse : public ros::Msg
  {
    public:
      typedef bool _success_type;
      _success_type success;
      uint32_t maps_length;
      typedef char* _maps_type;
      _maps_type st_maps;
      _maps_type * maps;

    ListMapsResponse():
      success(0),
      maps_length(0), maps(NULL)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.real = this->success;
      *(outbuffer + offset + 0) = (u_success.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->success);
      *(outbuffer + offset + 0) = (this->maps_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->maps_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->maps_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->maps_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->maps_length);
      for( uint32_t i = 0; i < maps_length; i++){
      uint32_t length_mapsi = strlen(this->maps[i]);
      varToArr(outbuffer + offset, length_mapsi);
      offset += 4;
      memcpy(outbuffer + offset, this->maps[i], length_mapsi);
      offset += length_mapsi;
      }
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.base = 0;
      u_success.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->success = u_success.real;
      offset += sizeof(this->success);
      uint32_t maps_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      maps_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      maps_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      maps_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->maps_length);
      if(maps_lengthT > maps_length)
        this->maps = (char**)realloc(this->maps, maps_lengthT * sizeof(char*));
      maps_length = maps_lengthT;
      for( uint32_t i = 0; i < maps_length; i++){
      uint32_t length_st_maps;
      arrToVar(length_st_maps, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_st_maps; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_st_maps-1]=0;
      this->st_maps = (char *)(inbuffer + offset-1);
      offset += length_st_maps;
        memcpy( &(this->maps[i]), &(this->st_maps), sizeof(char*));
      }
     return offset;
    }

    const char * getType(){ return LISTMAPS; };
    const char * getMD5(){ return "1ab49819563e07b1a3a5b6c08ee434d1"; };

  };

  class ListMaps {
    public:
    typedef ListMapsRequest Request;
    typedef ListMapsResponse Response;
  };

}
#endif
