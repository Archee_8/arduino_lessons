#ifndef _ROS_pal_interaction_msgs_Input_h
#define _ROS_pal_interaction_msgs_Input_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "pal_interaction_msgs/InputArgument.h"

namespace pal_interaction_msgs
{

  class Input : public ros::Msg
  {
    public:
      typedef const char* _action_type;
      _action_type action;
      uint32_t args_length;
      typedef pal_interaction_msgs::InputArgument _args_type;
      _args_type st_args;
      _args_type * args;
      enum { OK =  ok };
      enum { YES =  yes };
      enum { NO =  no };
      enum { CANCEL =  cancel };
      enum { ABORT =  abort };

    Input():
      action(""),
      args_length(0), args(NULL)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      uint32_t length_action = strlen(this->action);
      varToArr(outbuffer + offset, length_action);
      offset += 4;
      memcpy(outbuffer + offset, this->action, length_action);
      offset += length_action;
      *(outbuffer + offset + 0) = (this->args_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->args_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->args_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->args_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->args_length);
      for( uint32_t i = 0; i < args_length; i++){
      offset += this->args[i].serialize(outbuffer + offset);
      }
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      uint32_t length_action;
      arrToVar(length_action, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_action; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_action-1]=0;
      this->action = (char *)(inbuffer + offset-1);
      offset += length_action;
      uint32_t args_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      args_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      args_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      args_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->args_length);
      if(args_lengthT > args_length)
        this->args = (pal_interaction_msgs::InputArgument*)realloc(this->args, args_lengthT * sizeof(pal_interaction_msgs::InputArgument));
      args_length = args_lengthT;
      for( uint32_t i = 0; i < args_length; i++){
      offset += this->st_args.deserialize(inbuffer + offset);
        memcpy( &(this->args[i]), &(this->st_args), sizeof(pal_interaction_msgs::InputArgument));
      }
     return offset;
    }

    const char * getType(){ return "pal_interaction_msgs/Input"; };
    const char * getMD5(){ return "6a6c68e7abf2f4f861597e590594d6c4"; };

  };

}
#endif