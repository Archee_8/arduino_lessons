#ifndef _ROS_dynamic_introspection_IntrospectionMsg_h
#define _ROS_dynamic_introspection_IntrospectionMsg_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "std_msgs/Header.h"
#include "dynamic_introspection/BoolParameter.h"
#include "dynamic_introspection/DoubleParameter.h"
#include "dynamic_introspection/IntParameter.h"
#include "dynamic_introspection/MarkerParameter.h"

namespace dynamic_introspection
{

  class IntrospectionMsg : public ros::Msg
  {
    public:
      typedef std_msgs::Header _header_type;
      _header_type header;
      uint32_t bools_length;
      typedef dynamic_introspection::BoolParameter _bools_type;
      _bools_type st_bools;
      _bools_type * bools;
      uint32_t doubles_length;
      typedef dynamic_introspection::DoubleParameter _doubles_type;
      _doubles_type st_doubles;
      _doubles_type * doubles;
      uint32_t ints_length;
      typedef dynamic_introspection::IntParameter _ints_type;
      _ints_type st_ints;
      _ints_type * ints;
      uint32_t markers_length;
      typedef dynamic_introspection::MarkerParameter _markers_type;
      _markers_type st_markers;
      _markers_type * markers;

    IntrospectionMsg():
      header(),
      bools_length(0), bools(NULL),
      doubles_length(0), doubles(NULL),
      ints_length(0), ints(NULL),
      markers_length(0), markers(NULL)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->header.serialize(outbuffer + offset);
      *(outbuffer + offset + 0) = (this->bools_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->bools_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->bools_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->bools_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->bools_length);
      for( uint32_t i = 0; i < bools_length; i++){
      offset += this->bools[i].serialize(outbuffer + offset);
      }
      *(outbuffer + offset + 0) = (this->doubles_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->doubles_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->doubles_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->doubles_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->doubles_length);
      for( uint32_t i = 0; i < doubles_length; i++){
      offset += this->doubles[i].serialize(outbuffer + offset);
      }
      *(outbuffer + offset + 0) = (this->ints_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->ints_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->ints_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->ints_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->ints_length);
      for( uint32_t i = 0; i < ints_length; i++){
      offset += this->ints[i].serialize(outbuffer + offset);
      }
      *(outbuffer + offset + 0) = (this->markers_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->markers_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->markers_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->markers_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->markers_length);
      for( uint32_t i = 0; i < markers_length; i++){
      offset += this->markers[i].serialize(outbuffer + offset);
      }
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->header.deserialize(inbuffer + offset);
      uint32_t bools_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      bools_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      bools_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      bools_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->bools_length);
      if(bools_lengthT > bools_length)
        this->bools = (dynamic_introspection::BoolParameter*)realloc(this->bools, bools_lengthT * sizeof(dynamic_introspection::BoolParameter));
      bools_length = bools_lengthT;
      for( uint32_t i = 0; i < bools_length; i++){
      offset += this->st_bools.deserialize(inbuffer + offset);
        memcpy( &(this->bools[i]), &(this->st_bools), sizeof(dynamic_introspection::BoolParameter));
      }
      uint32_t doubles_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      doubles_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      doubles_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      doubles_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->doubles_length);
      if(doubles_lengthT > doubles_length)
        this->doubles = (dynamic_introspection::DoubleParameter*)realloc(this->doubles, doubles_lengthT * sizeof(dynamic_introspection::DoubleParameter));
      doubles_length = doubles_lengthT;
      for( uint32_t i = 0; i < doubles_length; i++){
      offset += this->st_doubles.deserialize(inbuffer + offset);
        memcpy( &(this->doubles[i]), &(this->st_doubles), sizeof(dynamic_introspection::DoubleParameter));
      }
      uint32_t ints_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      ints_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      ints_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      ints_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->ints_length);
      if(ints_lengthT > ints_length)
        this->ints = (dynamic_introspection::IntParameter*)realloc(this->ints, ints_lengthT * sizeof(dynamic_introspection::IntParameter));
      ints_length = ints_lengthT;
      for( uint32_t i = 0; i < ints_length; i++){
      offset += this->st_ints.deserialize(inbuffer + offset);
        memcpy( &(this->ints[i]), &(this->st_ints), sizeof(dynamic_introspection::IntParameter));
      }
      uint32_t markers_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      markers_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      markers_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      markers_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->markers_length);
      if(markers_lengthT > markers_length)
        this->markers = (dynamic_introspection::MarkerParameter*)realloc(this->markers, markers_lengthT * sizeof(dynamic_introspection::MarkerParameter));
      markers_length = markers_lengthT;
      for( uint32_t i = 0; i < markers_length; i++){
      offset += this->st_markers.deserialize(inbuffer + offset);
        memcpy( &(this->markers[i]), &(this->st_markers), sizeof(dynamic_introspection::MarkerParameter));
      }
     return offset;
    }

    const char * getType(){ return "dynamic_introspection/IntrospectionMsg"; };
    const char * getMD5(){ return "abf14c63c888d80e823c2b0710f2d3a3"; };

  };

}
#endif