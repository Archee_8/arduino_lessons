#ifndef _ROS_dynamic_introspection_MarkerParameter_h
#define _ROS_dynamic_introspection_MarkerParameter_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "visualization_msgs/MarkerArray.h"

namespace dynamic_introspection
{

  class MarkerParameter : public ros::Msg
  {
    public:
      typedef const char* _name_type;
      _name_type name;
      typedef visualization_msgs::MarkerArray _value_type;
      _value_type value;

    MarkerParameter():
      name(""),
      value()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      uint32_t length_name = strlen(this->name);
      varToArr(outbuffer + offset, length_name);
      offset += 4;
      memcpy(outbuffer + offset, this->name, length_name);
      offset += length_name;
      offset += this->value.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      uint32_t length_name;
      arrToVar(length_name, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_name; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_name-1]=0;
      this->name = (char *)(inbuffer + offset-1);
      offset += length_name;
      offset += this->value.deserialize(inbuffer + offset);
     return offset;
    }

    const char * getType(){ return "dynamic_introspection/MarkerParameter"; };
    const char * getMD5(){ return "320e3e3c740e97c435a8a239a215ff23"; };

  };

}
#endif