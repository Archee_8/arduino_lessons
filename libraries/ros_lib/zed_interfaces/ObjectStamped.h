#ifndef _ROS_zed_interfaces_ObjectStamped_h
#define _ROS_zed_interfaces_ObjectStamped_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "std_msgs/Header.h"
#include "geometry_msgs/Point32.h"
#include "geometry_msgs/Vector3.h"

namespace zed_interfaces
{

  class ObjectStamped : public ros::Msg
  {
    public:
      typedef std_msgs::Header _header_type;
      _header_type header;
      typedef const char* _label_type;
      _label_type label;
      typedef int16_t _label_id_type;
      _label_id_type label_id;
      typedef float _confidence_type;
      _confidence_type confidence;
      typedef geometry_msgs::Point32 _position_type;
      _position_type position;
      typedef geometry_msgs::Vector3 _linear_vel_type;
      _linear_vel_type linear_vel;
      typedef int8_t _tracking_state_type;
      _tracking_state_type tracking_state;
      geometry_msgs::Point32 bbox_2d[4];
      geometry_msgs::Point32 bbox_3d[8];

    ObjectStamped():
      header(),
      label(""),
      label_id(0),
      confidence(0),
      position(),
      linear_vel(),
      tracking_state(0),
      bbox_2d(),
      bbox_3d()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->header.serialize(outbuffer + offset);
      uint32_t length_label = strlen(this->label);
      varToArr(outbuffer + offset, length_label);
      offset += 4;
      memcpy(outbuffer + offset, this->label, length_label);
      offset += length_label;
      union {
        int16_t real;
        uint16_t base;
      } u_label_id;
      u_label_id.real = this->label_id;
      *(outbuffer + offset + 0) = (u_label_id.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_label_id.base >> (8 * 1)) & 0xFF;
      offset += sizeof(this->label_id);
      union {
        float real;
        uint32_t base;
      } u_confidence;
      u_confidence.real = this->confidence;
      *(outbuffer + offset + 0) = (u_confidence.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_confidence.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_confidence.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_confidence.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->confidence);
      offset += this->position.serialize(outbuffer + offset);
      offset += this->linear_vel.serialize(outbuffer + offset);
      union {
        int8_t real;
        uint8_t base;
      } u_tracking_state;
      u_tracking_state.real = this->tracking_state;
      *(outbuffer + offset + 0) = (u_tracking_state.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->tracking_state);
      for( uint32_t i = 0; i < 4; i++){
      offset += this->bbox_2d[i].serialize(outbuffer + offset);
      }
      for( uint32_t i = 0; i < 8; i++){
      offset += this->bbox_3d[i].serialize(outbuffer + offset);
      }
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->header.deserialize(inbuffer + offset);
      uint32_t length_label;
      arrToVar(length_label, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_label; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_label-1]=0;
      this->label = (char *)(inbuffer + offset-1);
      offset += length_label;
      union {
        int16_t real;
        uint16_t base;
      } u_label_id;
      u_label_id.base = 0;
      u_label_id.base |= ((uint16_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_label_id.base |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->label_id = u_label_id.real;
      offset += sizeof(this->label_id);
      union {
        float real;
        uint32_t base;
      } u_confidence;
      u_confidence.base = 0;
      u_confidence.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_confidence.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_confidence.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_confidence.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->confidence = u_confidence.real;
      offset += sizeof(this->confidence);
      offset += this->position.deserialize(inbuffer + offset);
      offset += this->linear_vel.deserialize(inbuffer + offset);
      union {
        int8_t real;
        uint8_t base;
      } u_tracking_state;
      u_tracking_state.base = 0;
      u_tracking_state.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->tracking_state = u_tracking_state.real;
      offset += sizeof(this->tracking_state);
      for( uint32_t i = 0; i < 4; i++){
      offset += this->bbox_2d[i].deserialize(inbuffer + offset);
      }
      for( uint32_t i = 0; i < 8; i++){
      offset += this->bbox_3d[i].deserialize(inbuffer + offset);
      }
     return offset;
    }

    const char * getType(){ return "zed_interfaces/ObjectStamped"; };
    const char * getMD5(){ return "d424656ac6d81bea041e4eb6457725ad"; };

  };

}
#endif