#ifndef _ROS_SERVICE_start_object_detection_h
#define _ROS_SERVICE_start_object_detection_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace zed_interfaces
{

static const char START_OBJECT_DETECTION[] = "zed_interfaces/start_object_detection";

  class start_object_detectionRequest : public ros::Msg
  {
    public:
      typedef float _confidence_type;
      _confidence_type confidence;
      typedef bool _tracking_type;
      _tracking_type tracking;
      typedef bool _people_type;
      _people_type people;
      typedef bool _vehicles_type;
      _vehicles_type vehicles;

    start_object_detectionRequest():
      confidence(0),
      tracking(0),
      people(0),
      vehicles(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        float real;
        uint32_t base;
      } u_confidence;
      u_confidence.real = this->confidence;
      *(outbuffer + offset + 0) = (u_confidence.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_confidence.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_confidence.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_confidence.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->confidence);
      union {
        bool real;
        uint8_t base;
      } u_tracking;
      u_tracking.real = this->tracking;
      *(outbuffer + offset + 0) = (u_tracking.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->tracking);
      union {
        bool real;
        uint8_t base;
      } u_people;
      u_people.real = this->people;
      *(outbuffer + offset + 0) = (u_people.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->people);
      union {
        bool real;
        uint8_t base;
      } u_vehicles;
      u_vehicles.real = this->vehicles;
      *(outbuffer + offset + 0) = (u_vehicles.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->vehicles);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        float real;
        uint32_t base;
      } u_confidence;
      u_confidence.base = 0;
      u_confidence.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_confidence.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_confidence.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_confidence.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->confidence = u_confidence.real;
      offset += sizeof(this->confidence);
      union {
        bool real;
        uint8_t base;
      } u_tracking;
      u_tracking.base = 0;
      u_tracking.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->tracking = u_tracking.real;
      offset += sizeof(this->tracking);
      union {
        bool real;
        uint8_t base;
      } u_people;
      u_people.base = 0;
      u_people.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->people = u_people.real;
      offset += sizeof(this->people);
      union {
        bool real;
        uint8_t base;
      } u_vehicles;
      u_vehicles.base = 0;
      u_vehicles.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->vehicles = u_vehicles.real;
      offset += sizeof(this->vehicles);
     return offset;
    }

    const char * getType(){ return START_OBJECT_DETECTION; };
    const char * getMD5(){ return "d390c3c6cd39e296e71a58be92b33ec6"; };

  };

  class start_object_detectionResponse : public ros::Msg
  {
    public:
      typedef bool _done_type;
      _done_type done;

    start_object_detectionResponse():
      done(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_done;
      u_done.real = this->done;
      *(outbuffer + offset + 0) = (u_done.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->done);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_done;
      u_done.base = 0;
      u_done.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->done = u_done.real;
      offset += sizeof(this->done);
     return offset;
    }

    const char * getType(){ return START_OBJECT_DETECTION; };
    const char * getMD5(){ return "89bb254424e4cffedbf494e7b0ddbfea"; };

  };

  class start_object_detection {
    public:
    typedef start_object_detectionRequest Request;
    typedef start_object_detectionResponse Response;
  };

}
#endif
