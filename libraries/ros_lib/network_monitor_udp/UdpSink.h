#ifndef _ROS_network_monitor_udp_UdpSink_h
#define _ROS_network_monitor_udp_UdpSink_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace network_monitor_udp
{

  class UdpSink : public ros::Msg
  {
    public:
      uint8_t magic[4];
      typedef float _send_time_type;
      _send_time_type send_time;
      typedef float _echo_time_type;
      _echo_time_type echo_time;
      typedef int32_t _seqnum_type;
      _seqnum_type seqnum;
      typedef int32_t _source_id_type;
      _source_id_type source_id;

    UdpSink():
      magic(),
      send_time(0),
      echo_time(0),
      seqnum(0),
      source_id(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      for( uint32_t i = 0; i < 4; i++){
      *(outbuffer + offset + 0) = (this->magic[i] >> (8 * 0)) & 0xFF;
      offset += sizeof(this->magic[i]);
      }
      offset += serializeAvrFloat64(outbuffer + offset, this->send_time);
      offset += serializeAvrFloat64(outbuffer + offset, this->echo_time);
      union {
        int32_t real;
        uint32_t base;
      } u_seqnum;
      u_seqnum.real = this->seqnum;
      *(outbuffer + offset + 0) = (u_seqnum.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_seqnum.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_seqnum.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_seqnum.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->seqnum);
      union {
        int32_t real;
        uint32_t base;
      } u_source_id;
      u_source_id.real = this->source_id;
      *(outbuffer + offset + 0) = (u_source_id.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_source_id.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_source_id.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_source_id.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->source_id);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      for( uint32_t i = 0; i < 4; i++){
      this->magic[i] =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->magic[i]);
      }
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->send_time));
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->echo_time));
      union {
        int32_t real;
        uint32_t base;
      } u_seqnum;
      u_seqnum.base = 0;
      u_seqnum.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_seqnum.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_seqnum.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_seqnum.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->seqnum = u_seqnum.real;
      offset += sizeof(this->seqnum);
      union {
        int32_t real;
        uint32_t base;
      } u_source_id;
      u_source_id.base = 0;
      u_source_id.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_source_id.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_source_id.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_source_id.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->source_id = u_source_id.real;
      offset += sizeof(this->source_id);
     return offset;
    }

    const char * getType(){ return "network_monitor_udp/UdpSink"; };
    const char * getMD5(){ return "a56e2a33942a368e87b357cc9e894ec5"; };

  };

}
#endif