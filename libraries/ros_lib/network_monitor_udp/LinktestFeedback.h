#ifndef _ROS_network_monitor_udp_LinktestFeedback_h
#define _ROS_network_monitor_udp_LinktestFeedback_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "ros/time.h"

namespace network_monitor_udp
{

  class LinktestFeedback : public ros::Msg
  {
    public:
      typedef float _latency_type;
      _latency_type latency;
      typedef float _loss_type;
      _loss_type loss;
      typedef float _bandwidth_type;
      _bandwidth_type bandwidth;
      uint32_t latency_histogram_length;
      typedef float _latency_histogram_type;
      _latency_histogram_type st_latency_histogram;
      _latency_histogram_type * latency_histogram;
      typedef ros::Time _stamp_type;
      _stamp_type stamp;

    LinktestFeedback():
      latency(0),
      loss(0),
      bandwidth(0),
      latency_histogram_length(0), latency_histogram(NULL),
      stamp()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        float real;
        uint32_t base;
      } u_latency;
      u_latency.real = this->latency;
      *(outbuffer + offset + 0) = (u_latency.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_latency.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_latency.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_latency.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->latency);
      union {
        float real;
        uint32_t base;
      } u_loss;
      u_loss.real = this->loss;
      *(outbuffer + offset + 0) = (u_loss.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_loss.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_loss.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_loss.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->loss);
      union {
        float real;
        uint32_t base;
      } u_bandwidth;
      u_bandwidth.real = this->bandwidth;
      *(outbuffer + offset + 0) = (u_bandwidth.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_bandwidth.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_bandwidth.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_bandwidth.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->bandwidth);
      *(outbuffer + offset + 0) = (this->latency_histogram_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->latency_histogram_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->latency_histogram_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->latency_histogram_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->latency_histogram_length);
      for( uint32_t i = 0; i < latency_histogram_length; i++){
      union {
        float real;
        uint32_t base;
      } u_latency_histogrami;
      u_latency_histogrami.real = this->latency_histogram[i];
      *(outbuffer + offset + 0) = (u_latency_histogrami.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_latency_histogrami.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_latency_histogrami.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_latency_histogrami.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->latency_histogram[i]);
      }
      *(outbuffer + offset + 0) = (this->stamp.sec >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->stamp.sec >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->stamp.sec >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->stamp.sec >> (8 * 3)) & 0xFF;
      offset += sizeof(this->stamp.sec);
      *(outbuffer + offset + 0) = (this->stamp.nsec >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->stamp.nsec >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->stamp.nsec >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->stamp.nsec >> (8 * 3)) & 0xFF;
      offset += sizeof(this->stamp.nsec);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        float real;
        uint32_t base;
      } u_latency;
      u_latency.base = 0;
      u_latency.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_latency.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_latency.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_latency.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->latency = u_latency.real;
      offset += sizeof(this->latency);
      union {
        float real;
        uint32_t base;
      } u_loss;
      u_loss.base = 0;
      u_loss.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_loss.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_loss.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_loss.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->loss = u_loss.real;
      offset += sizeof(this->loss);
      union {
        float real;
        uint32_t base;
      } u_bandwidth;
      u_bandwidth.base = 0;
      u_bandwidth.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_bandwidth.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_bandwidth.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_bandwidth.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->bandwidth = u_bandwidth.real;
      offset += sizeof(this->bandwidth);
      uint32_t latency_histogram_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      latency_histogram_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      latency_histogram_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      latency_histogram_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->latency_histogram_length);
      if(latency_histogram_lengthT > latency_histogram_length)
        this->latency_histogram = (float*)realloc(this->latency_histogram, latency_histogram_lengthT * sizeof(float));
      latency_histogram_length = latency_histogram_lengthT;
      for( uint32_t i = 0; i < latency_histogram_length; i++){
      union {
        float real;
        uint32_t base;
      } u_st_latency_histogram;
      u_st_latency_histogram.base = 0;
      u_st_latency_histogram.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_st_latency_histogram.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_st_latency_histogram.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_st_latency_histogram.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->st_latency_histogram = u_st_latency_histogram.real;
      offset += sizeof(this->st_latency_histogram);
        memcpy( &(this->latency_histogram[i]), &(this->st_latency_histogram), sizeof(float));
      }
      this->stamp.sec =  ((uint32_t) (*(inbuffer + offset)));
      this->stamp.sec |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->stamp.sec |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      this->stamp.sec |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      offset += sizeof(this->stamp.sec);
      this->stamp.nsec =  ((uint32_t) (*(inbuffer + offset)));
      this->stamp.nsec |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->stamp.nsec |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      this->stamp.nsec |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      offset += sizeof(this->stamp.nsec);
     return offset;
    }

    const char * getType(){ return "network_monitor_udp/LinktestFeedback"; };
    const char * getMD5(){ return "0692c53722c7e369a7f8108c019796f3"; };

  };

}
#endif