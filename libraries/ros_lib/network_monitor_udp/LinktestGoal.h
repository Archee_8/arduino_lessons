#ifndef _ROS_network_monitor_udp_LinktestGoal_h
#define _ROS_network_monitor_udp_LinktestGoal_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace network_monitor_udp
{

  class LinktestGoal : public ros::Msg
  {
    public:
      typedef float _duration_type;
      _duration_type duration;
      typedef float _update_interval_type;
      _update_interval_type update_interval;
      typedef float _bw_type;
      _bw_type bw;
      typedef uint8_t _bw_type_type;
      _bw_type_type bw_type;
      typedef float _latency_threshold_type;
      _latency_threshold_type latency_threshold;
      typedef float _pktloss_threshold_type;
      _pktloss_threshold_type pktloss_threshold;
      typedef uint8_t _tos_type;
      _tos_type tos;
      typedef int32_t _pktsize_type;
      _pktsize_type pktsize;
      typedef bool _ros_returnpath_type;
      _ros_returnpath_type ros_returnpath;
      typedef bool _roundtrip_type;
      _roundtrip_type roundtrip;
      typedef float _max_return_time_type;
      _max_return_time_type max_return_time;
      typedef const char* _rostopic_prefix_type;
      _rostopic_prefix_type rostopic_prefix;
      typedef const char* _sink_ip_type;
      _sink_ip_type sink_ip;
      typedef int32_t _sink_port_type;
      _sink_port_type sink_port;
      uint32_t latencybins_length;
      typedef float _latencybins_type;
      _latencybins_type st_latencybins;
      _latencybins_type * latencybins;
      enum { DEFAULT_UPDATE_INTERVAL = 0.15     };
      enum { DEFAULT_BW = 5000000.0             };
      enum { DEFAULT_BWTYPE = 99                   };
      enum { BW_CONSTANT = 99                      };
      enum { BW_ADAPTIVE = 97                      };
      enum { LIM1 = -0.3                        };
      enum { LIM2 = 0.3 };
      enum { C1 = -0.05 };
      enum { C2 = 0.2 };
      enum { DEFAULT_LATENCY_THRESHOLD = 0.01   };
      enum { DEFAULT_PKTLOSS_THRESHOLD = 0.5    };
      enum { DEFAULT_PKTSIZE = 1500               };

    LinktestGoal():
      duration(0),
      update_interval(0),
      bw(0),
      bw_type(0),
      latency_threshold(0),
      pktloss_threshold(0),
      tos(0),
      pktsize(0),
      ros_returnpath(0),
      roundtrip(0),
      max_return_time(0),
      rostopic_prefix(""),
      sink_ip(""),
      sink_port(0),
      latencybins_length(0), latencybins(NULL)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        float real;
        uint32_t base;
      } u_duration;
      u_duration.real = this->duration;
      *(outbuffer + offset + 0) = (u_duration.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_duration.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_duration.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_duration.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->duration);
      union {
        float real;
        uint32_t base;
      } u_update_interval;
      u_update_interval.real = this->update_interval;
      *(outbuffer + offset + 0) = (u_update_interval.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_update_interval.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_update_interval.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_update_interval.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->update_interval);
      union {
        float real;
        uint32_t base;
      } u_bw;
      u_bw.real = this->bw;
      *(outbuffer + offset + 0) = (u_bw.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_bw.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_bw.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_bw.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->bw);
      *(outbuffer + offset + 0) = (this->bw_type >> (8 * 0)) & 0xFF;
      offset += sizeof(this->bw_type);
      union {
        float real;
        uint32_t base;
      } u_latency_threshold;
      u_latency_threshold.real = this->latency_threshold;
      *(outbuffer + offset + 0) = (u_latency_threshold.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_latency_threshold.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_latency_threshold.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_latency_threshold.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->latency_threshold);
      union {
        float real;
        uint32_t base;
      } u_pktloss_threshold;
      u_pktloss_threshold.real = this->pktloss_threshold;
      *(outbuffer + offset + 0) = (u_pktloss_threshold.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_pktloss_threshold.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_pktloss_threshold.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_pktloss_threshold.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->pktloss_threshold);
      *(outbuffer + offset + 0) = (this->tos >> (8 * 0)) & 0xFF;
      offset += sizeof(this->tos);
      union {
        int32_t real;
        uint32_t base;
      } u_pktsize;
      u_pktsize.real = this->pktsize;
      *(outbuffer + offset + 0) = (u_pktsize.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_pktsize.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_pktsize.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_pktsize.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->pktsize);
      union {
        bool real;
        uint8_t base;
      } u_ros_returnpath;
      u_ros_returnpath.real = this->ros_returnpath;
      *(outbuffer + offset + 0) = (u_ros_returnpath.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->ros_returnpath);
      union {
        bool real;
        uint8_t base;
      } u_roundtrip;
      u_roundtrip.real = this->roundtrip;
      *(outbuffer + offset + 0) = (u_roundtrip.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->roundtrip);
      union {
        float real;
        uint32_t base;
      } u_max_return_time;
      u_max_return_time.real = this->max_return_time;
      *(outbuffer + offset + 0) = (u_max_return_time.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_max_return_time.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_max_return_time.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_max_return_time.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->max_return_time);
      uint32_t length_rostopic_prefix = strlen(this->rostopic_prefix);
      varToArr(outbuffer + offset, length_rostopic_prefix);
      offset += 4;
      memcpy(outbuffer + offset, this->rostopic_prefix, length_rostopic_prefix);
      offset += length_rostopic_prefix;
      uint32_t length_sink_ip = strlen(this->sink_ip);
      varToArr(outbuffer + offset, length_sink_ip);
      offset += 4;
      memcpy(outbuffer + offset, this->sink_ip, length_sink_ip);
      offset += length_sink_ip;
      union {
        int32_t real;
        uint32_t base;
      } u_sink_port;
      u_sink_port.real = this->sink_port;
      *(outbuffer + offset + 0) = (u_sink_port.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_sink_port.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_sink_port.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_sink_port.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->sink_port);
      *(outbuffer + offset + 0) = (this->latencybins_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->latencybins_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->latencybins_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->latencybins_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->latencybins_length);
      for( uint32_t i = 0; i < latencybins_length; i++){
      union {
        float real;
        uint32_t base;
      } u_latencybinsi;
      u_latencybinsi.real = this->latencybins[i];
      *(outbuffer + offset + 0) = (u_latencybinsi.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_latencybinsi.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_latencybinsi.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_latencybinsi.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->latencybins[i]);
      }
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        float real;
        uint32_t base;
      } u_duration;
      u_duration.base = 0;
      u_duration.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_duration.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_duration.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_duration.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->duration = u_duration.real;
      offset += sizeof(this->duration);
      union {
        float real;
        uint32_t base;
      } u_update_interval;
      u_update_interval.base = 0;
      u_update_interval.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_update_interval.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_update_interval.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_update_interval.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->update_interval = u_update_interval.real;
      offset += sizeof(this->update_interval);
      union {
        float real;
        uint32_t base;
      } u_bw;
      u_bw.base = 0;
      u_bw.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_bw.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_bw.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_bw.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->bw = u_bw.real;
      offset += sizeof(this->bw);
      this->bw_type =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->bw_type);
      union {
        float real;
        uint32_t base;
      } u_latency_threshold;
      u_latency_threshold.base = 0;
      u_latency_threshold.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_latency_threshold.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_latency_threshold.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_latency_threshold.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->latency_threshold = u_latency_threshold.real;
      offset += sizeof(this->latency_threshold);
      union {
        float real;
        uint32_t base;
      } u_pktloss_threshold;
      u_pktloss_threshold.base = 0;
      u_pktloss_threshold.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_pktloss_threshold.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_pktloss_threshold.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_pktloss_threshold.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->pktloss_threshold = u_pktloss_threshold.real;
      offset += sizeof(this->pktloss_threshold);
      this->tos =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->tos);
      union {
        int32_t real;
        uint32_t base;
      } u_pktsize;
      u_pktsize.base = 0;
      u_pktsize.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_pktsize.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_pktsize.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_pktsize.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->pktsize = u_pktsize.real;
      offset += sizeof(this->pktsize);
      union {
        bool real;
        uint8_t base;
      } u_ros_returnpath;
      u_ros_returnpath.base = 0;
      u_ros_returnpath.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->ros_returnpath = u_ros_returnpath.real;
      offset += sizeof(this->ros_returnpath);
      union {
        bool real;
        uint8_t base;
      } u_roundtrip;
      u_roundtrip.base = 0;
      u_roundtrip.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->roundtrip = u_roundtrip.real;
      offset += sizeof(this->roundtrip);
      union {
        float real;
        uint32_t base;
      } u_max_return_time;
      u_max_return_time.base = 0;
      u_max_return_time.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_max_return_time.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_max_return_time.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_max_return_time.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->max_return_time = u_max_return_time.real;
      offset += sizeof(this->max_return_time);
      uint32_t length_rostopic_prefix;
      arrToVar(length_rostopic_prefix, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_rostopic_prefix; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_rostopic_prefix-1]=0;
      this->rostopic_prefix = (char *)(inbuffer + offset-1);
      offset += length_rostopic_prefix;
      uint32_t length_sink_ip;
      arrToVar(length_sink_ip, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_sink_ip; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_sink_ip-1]=0;
      this->sink_ip = (char *)(inbuffer + offset-1);
      offset += length_sink_ip;
      union {
        int32_t real;
        uint32_t base;
      } u_sink_port;
      u_sink_port.base = 0;
      u_sink_port.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_sink_port.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_sink_port.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_sink_port.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->sink_port = u_sink_port.real;
      offset += sizeof(this->sink_port);
      uint32_t latencybins_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      latencybins_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      latencybins_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      latencybins_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->latencybins_length);
      if(latencybins_lengthT > latencybins_length)
        this->latencybins = (float*)realloc(this->latencybins, latencybins_lengthT * sizeof(float));
      latencybins_length = latencybins_lengthT;
      for( uint32_t i = 0; i < latencybins_length; i++){
      union {
        float real;
        uint32_t base;
      } u_st_latencybins;
      u_st_latencybins.base = 0;
      u_st_latencybins.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_st_latencybins.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_st_latencybins.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_st_latencybins.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->st_latencybins = u_st_latencybins.real;
      offset += sizeof(this->st_latencybins);
        memcpy( &(this->latencybins[i]), &(this->st_latencybins), sizeof(float));
      }
     return offset;
    }

    const char * getType(){ return "network_monitor_udp/LinktestGoal"; };
    const char * getMD5(){ return "a319f2787ce16837363051a27c7fd49f"; };

  };

}
#endif