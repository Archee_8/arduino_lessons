#ifndef _ROS_pal_statistics_msgs_Statistics_h
#define _ROS_pal_statistics_msgs_Statistics_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "std_msgs/Header.h"
#include "pal_statistics_msgs/Statistic.h"

namespace pal_statistics_msgs
{

  class Statistics : public ros::Msg
  {
    public:
      typedef std_msgs::Header _header_type;
      _header_type header;
      uint32_t statistics_length;
      typedef pal_statistics_msgs::Statistic _statistics_type;
      _statistics_type st_statistics;
      _statistics_type * statistics;

    Statistics():
      header(),
      statistics_length(0), statistics(NULL)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->header.serialize(outbuffer + offset);
      *(outbuffer + offset + 0) = (this->statistics_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->statistics_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->statistics_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->statistics_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->statistics_length);
      for( uint32_t i = 0; i < statistics_length; i++){
      offset += this->statistics[i].serialize(outbuffer + offset);
      }
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->header.deserialize(inbuffer + offset);
      uint32_t statistics_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      statistics_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      statistics_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      statistics_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->statistics_length);
      if(statistics_lengthT > statistics_length)
        this->statistics = (pal_statistics_msgs::Statistic*)realloc(this->statistics, statistics_lengthT * sizeof(pal_statistics_msgs::Statistic));
      statistics_length = statistics_lengthT;
      for( uint32_t i = 0; i < statistics_length; i++){
      offset += this->st_statistics.deserialize(inbuffer + offset);
        memcpy( &(this->statistics[i]), &(this->st_statistics), sizeof(pal_statistics_msgs::Statistic));
      }
     return offset;
    }

    const char * getType(){ return "pal_statistics_msgs/Statistics"; };
    const char * getMD5(){ return "3f331753b5cb45fe4c2ecf2b2c5f78ac"; };

  };

}
#endif