#ifndef _ROS_SERVICE_MapSrv_h
#define _ROS_SERVICE_MapSrv_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "nav_msgs/OccupancyGrid.h"

namespace hw4
{

static const char MAPSRV[] = "hw4/MapSrv";

  class MapSrvRequest : public ros::Msg
  {
    public:
      typedef nav_msgs::OccupancyGrid _map_in_type;
      _map_in_type map_in;

    MapSrvRequest():
      map_in()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->map_in.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->map_in.deserialize(inbuffer + offset);
     return offset;
    }

    const char * getType(){ return MAPSRV; };
    const char * getMD5(){ return "b6cd2295cd636fca9c09e1d8704e6ec1"; };

  };

  class MapSrvResponse : public ros::Msg
  {
    public:
      typedef nav_msgs::OccupancyGrid _map_out_type;
      _map_out_type map_out;

    MapSrvResponse():
      map_out()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->map_out.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->map_out.deserialize(inbuffer + offset);
     return offset;
    }

    const char * getType(){ return MAPSRV; };
    const char * getMD5(){ return "ad7638d9db6a9d430f31db5776d540d6"; };

  };

  class MapSrv {
    public:
    typedef MapSrvRequest Request;
    typedef MapSrvResponse Response;
  };

}
#endif
