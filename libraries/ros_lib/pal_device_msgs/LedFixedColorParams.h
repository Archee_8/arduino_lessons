#ifndef _ROS_pal_device_msgs_LedFixedColorParams_h
#define _ROS_pal_device_msgs_LedFixedColorParams_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "std_msgs/ColorRGBA.h"

namespace pal_device_msgs
{

  class LedFixedColorParams : public ros::Msg
  {
    public:
      typedef std_msgs::ColorRGBA _color_type;
      _color_type color;

    LedFixedColorParams():
      color()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->color.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->color.deserialize(inbuffer + offset);
     return offset;
    }

    const char * getType(){ return "pal_device_msgs/LedFixedColorParams"; };
    const char * getMD5(){ return "3e04b62b1b39cd97e873789f0bb130e7"; };

  };

}
#endif