#ifndef _ROS_pal_device_msgs_LedProgressParams_h
#define _ROS_pal_device_msgs_LedProgressParams_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "std_msgs/ColorRGBA.h"

namespace pal_device_msgs
{

  class LedProgressParams : public ros::Msg
  {
    public:
      typedef std_msgs::ColorRGBA _first_color_type;
      _first_color_type first_color;
      typedef std_msgs::ColorRGBA _second_color_type;
      _second_color_type second_color;
      typedef float _percentage_type;
      _percentage_type percentage;
      typedef float _led_offset_type;
      _led_offset_type led_offset;

    LedProgressParams():
      first_color(),
      second_color(),
      percentage(0),
      led_offset(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->first_color.serialize(outbuffer + offset);
      offset += this->second_color.serialize(outbuffer + offset);
      union {
        float real;
        uint32_t base;
      } u_percentage;
      u_percentage.real = this->percentage;
      *(outbuffer + offset + 0) = (u_percentage.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_percentage.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_percentage.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_percentage.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->percentage);
      union {
        float real;
        uint32_t base;
      } u_led_offset;
      u_led_offset.real = this->led_offset;
      *(outbuffer + offset + 0) = (u_led_offset.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_led_offset.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_led_offset.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_led_offset.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->led_offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->first_color.deserialize(inbuffer + offset);
      offset += this->second_color.deserialize(inbuffer + offset);
      union {
        float real;
        uint32_t base;
      } u_percentage;
      u_percentage.base = 0;
      u_percentage.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_percentage.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_percentage.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_percentage.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->percentage = u_percentage.real;
      offset += sizeof(this->percentage);
      union {
        float real;
        uint32_t base;
      } u_led_offset;
      u_led_offset.base = 0;
      u_led_offset.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_led_offset.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_led_offset.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_led_offset.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->led_offset = u_led_offset.real;
      offset += sizeof(this->led_offset);
     return offset;
    }

    const char * getType(){ return "pal_device_msgs/LedProgressParams"; };
    const char * getMD5(){ return "941e895db6a3875d51a22a0a591d753a"; };

  };

}
#endif