#ifndef _ROS_pal_device_msgs_LedRainbowParams_h
#define _ROS_pal_device_msgs_LedRainbowParams_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "ros/duration.h"

namespace pal_device_msgs
{

  class LedRainbowParams : public ros::Msg
  {
    public:
      typedef ros::Duration _transition_duration_type;
      _transition_duration_type transition_duration;

    LedRainbowParams():
      transition_duration()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      *(outbuffer + offset + 0) = (this->transition_duration.sec >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->transition_duration.sec >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->transition_duration.sec >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->transition_duration.sec >> (8 * 3)) & 0xFF;
      offset += sizeof(this->transition_duration.sec);
      *(outbuffer + offset + 0) = (this->transition_duration.nsec >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->transition_duration.nsec >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->transition_duration.nsec >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->transition_duration.nsec >> (8 * 3)) & 0xFF;
      offset += sizeof(this->transition_duration.nsec);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      this->transition_duration.sec =  ((uint32_t) (*(inbuffer + offset)));
      this->transition_duration.sec |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->transition_duration.sec |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      this->transition_duration.sec |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      offset += sizeof(this->transition_duration.sec);
      this->transition_duration.nsec =  ((uint32_t) (*(inbuffer + offset)));
      this->transition_duration.nsec |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->transition_duration.nsec |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      this->transition_duration.nsec |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      offset += sizeof(this->transition_duration.nsec);
     return offset;
    }

    const char * getType(){ return "pal_device_msgs/LedRainbowParams"; };
    const char * getMD5(){ return "f44f9582c55f88a544d929daa76e5ae7"; };

  };

}
#endif