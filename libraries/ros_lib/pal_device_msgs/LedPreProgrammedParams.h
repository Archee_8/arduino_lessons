#ifndef _ROS_pal_device_msgs_LedPreProgrammedParams_h
#define _ROS_pal_device_msgs_LedPreProgrammedParams_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace pal_device_msgs
{

  class LedPreProgrammedParams : public ros::Msg
  {
    public:
      typedef uint8_t _preprogrammed_id_type;
      _preprogrammed_id_type preprogrammed_id;

    LedPreProgrammedParams():
      preprogrammed_id(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      *(outbuffer + offset + 0) = (this->preprogrammed_id >> (8 * 0)) & 0xFF;
      offset += sizeof(this->preprogrammed_id);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      this->preprogrammed_id =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->preprogrammed_id);
     return offset;
    }

    const char * getType(){ return "pal_device_msgs/LedPreProgrammedParams"; };
    const char * getMD5(){ return "269ccb5728503bb2fd4261e5b289d3c1"; };

  };

}
#endif