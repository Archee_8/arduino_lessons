#ifndef _ROS_pal_device_msgs_LedEffectParams_h
#define _ROS_pal_device_msgs_LedEffectParams_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "pal_device_msgs/LedFixedColorParams.h"
#include "pal_device_msgs/LedRainbowParams.h"
#include "pal_device_msgs/LedFadeParams.h"
#include "pal_device_msgs/LedBlinkParams.h"
#include "pal_device_msgs/LedProgressParams.h"
#include "pal_device_msgs/LedFlowParams.h"
#include "pal_device_msgs/LedPreProgrammedParams.h"
#include "pal_device_msgs/LedEffectViaTopicParams.h"
#include "pal_device_msgs/LedDataArrayParams.h"

namespace pal_device_msgs
{

  class LedEffectParams : public ros::Msg
  {
    public:
      typedef uint8_t _effectType_type;
      _effectType_type effectType;
      typedef pal_device_msgs::LedFixedColorParams _fixed_color_type;
      _fixed_color_type fixed_color;
      typedef pal_device_msgs::LedRainbowParams _rainbow_type;
      _rainbow_type rainbow;
      typedef pal_device_msgs::LedFadeParams _fade_type;
      _fade_type fade;
      typedef pal_device_msgs::LedBlinkParams _blink_type;
      _blink_type blink;
      typedef pal_device_msgs::LedProgressParams _progress_type;
      _progress_type progress;
      typedef pal_device_msgs::LedFlowParams _flow_type;
      _flow_type flow;
      typedef pal_device_msgs::LedPreProgrammedParams _preprogrammed_type;
      _preprogrammed_type preprogrammed;
      typedef pal_device_msgs::LedEffectViaTopicParams _effect_via_topic_type;
      _effect_via_topic_type effect_via_topic;
      typedef pal_device_msgs::LedDataArrayParams _data_array_type;
      _data_array_type data_array;
      enum { FIXED_COLOR = 0 };
      enum { RAINBOW = 1 };
      enum { FADE = 2 };
      enum { BLINK = 3 };
      enum { PROGRESS = 4 };
      enum { FLOW = 5 };
      enum { PREPROGRAMMED_EFFECT = 6 };
      enum { EFFECT_VIA_TOPIC = 7 };
      enum { DATA_ARRAY = 8 };

    LedEffectParams():
      effectType(0),
      fixed_color(),
      rainbow(),
      fade(),
      blink(),
      progress(),
      flow(),
      preprogrammed(),
      effect_via_topic(),
      data_array()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      *(outbuffer + offset + 0) = (this->effectType >> (8 * 0)) & 0xFF;
      offset += sizeof(this->effectType);
      offset += this->fixed_color.serialize(outbuffer + offset);
      offset += this->rainbow.serialize(outbuffer + offset);
      offset += this->fade.serialize(outbuffer + offset);
      offset += this->blink.serialize(outbuffer + offset);
      offset += this->progress.serialize(outbuffer + offset);
      offset += this->flow.serialize(outbuffer + offset);
      offset += this->preprogrammed.serialize(outbuffer + offset);
      offset += this->effect_via_topic.serialize(outbuffer + offset);
      offset += this->data_array.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      this->effectType =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->effectType);
      offset += this->fixed_color.deserialize(inbuffer + offset);
      offset += this->rainbow.deserialize(inbuffer + offset);
      offset += this->fade.deserialize(inbuffer + offset);
      offset += this->blink.deserialize(inbuffer + offset);
      offset += this->progress.deserialize(inbuffer + offset);
      offset += this->flow.deserialize(inbuffer + offset);
      offset += this->preprogrammed.deserialize(inbuffer + offset);
      offset += this->effect_via_topic.deserialize(inbuffer + offset);
      offset += this->data_array.deserialize(inbuffer + offset);
     return offset;
    }

    const char * getType(){ return "pal_device_msgs/LedEffectParams"; };
    const char * getMD5(){ return "cf78e4edf6ae7c6ce024d091a1a3c19a"; };

  };

}
#endif