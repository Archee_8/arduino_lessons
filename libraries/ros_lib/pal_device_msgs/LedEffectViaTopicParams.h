#ifndef _ROS_pal_device_msgs_LedEffectViaTopicParams_h
#define _ROS_pal_device_msgs_LedEffectViaTopicParams_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace pal_device_msgs
{

  class LedEffectViaTopicParams : public ros::Msg
  {
    public:
      typedef const char* _topic_name_type;
      _topic_name_type topic_name;

    LedEffectViaTopicParams():
      topic_name("")
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      uint32_t length_topic_name = strlen(this->topic_name);
      varToArr(outbuffer + offset, length_topic_name);
      offset += 4;
      memcpy(outbuffer + offset, this->topic_name, length_topic_name);
      offset += length_topic_name;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      uint32_t length_topic_name;
      arrToVar(length_topic_name, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_topic_name; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_topic_name-1]=0;
      this->topic_name = (char *)(inbuffer + offset-1);
      offset += length_topic_name;
     return offset;
    }

    const char * getType(){ return "pal_device_msgs/LedEffectViaTopicParams"; };
    const char * getMD5(){ return "b38cc2f19f45368c2db7867751ce95a9"; };

  };

}
#endif