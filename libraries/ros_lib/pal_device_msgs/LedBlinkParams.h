#ifndef _ROS_pal_device_msgs_LedBlinkParams_h
#define _ROS_pal_device_msgs_LedBlinkParams_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "std_msgs/ColorRGBA.h"
#include "ros/duration.h"

namespace pal_device_msgs
{

  class LedBlinkParams : public ros::Msg
  {
    public:
      typedef std_msgs::ColorRGBA _first_color_type;
      _first_color_type first_color;
      typedef std_msgs::ColorRGBA _second_color_type;
      _second_color_type second_color;
      typedef ros::Duration _first_color_duration_type;
      _first_color_duration_type first_color_duration;
      typedef ros::Duration _second_color_duration_type;
      _second_color_duration_type second_color_duration;

    LedBlinkParams():
      first_color(),
      second_color(),
      first_color_duration(),
      second_color_duration()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->first_color.serialize(outbuffer + offset);
      offset += this->second_color.serialize(outbuffer + offset);
      *(outbuffer + offset + 0) = (this->first_color_duration.sec >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->first_color_duration.sec >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->first_color_duration.sec >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->first_color_duration.sec >> (8 * 3)) & 0xFF;
      offset += sizeof(this->first_color_duration.sec);
      *(outbuffer + offset + 0) = (this->first_color_duration.nsec >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->first_color_duration.nsec >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->first_color_duration.nsec >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->first_color_duration.nsec >> (8 * 3)) & 0xFF;
      offset += sizeof(this->first_color_duration.nsec);
      *(outbuffer + offset + 0) = (this->second_color_duration.sec >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->second_color_duration.sec >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->second_color_duration.sec >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->second_color_duration.sec >> (8 * 3)) & 0xFF;
      offset += sizeof(this->second_color_duration.sec);
      *(outbuffer + offset + 0) = (this->second_color_duration.nsec >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->second_color_duration.nsec >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->second_color_duration.nsec >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->second_color_duration.nsec >> (8 * 3)) & 0xFF;
      offset += sizeof(this->second_color_duration.nsec);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->first_color.deserialize(inbuffer + offset);
      offset += this->second_color.deserialize(inbuffer + offset);
      this->first_color_duration.sec =  ((uint32_t) (*(inbuffer + offset)));
      this->first_color_duration.sec |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->first_color_duration.sec |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      this->first_color_duration.sec |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      offset += sizeof(this->first_color_duration.sec);
      this->first_color_duration.nsec =  ((uint32_t) (*(inbuffer + offset)));
      this->first_color_duration.nsec |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->first_color_duration.nsec |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      this->first_color_duration.nsec |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      offset += sizeof(this->first_color_duration.nsec);
      this->second_color_duration.sec =  ((uint32_t) (*(inbuffer + offset)));
      this->second_color_duration.sec |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->second_color_duration.sec |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      this->second_color_duration.sec |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      offset += sizeof(this->second_color_duration.sec);
      this->second_color_duration.nsec =  ((uint32_t) (*(inbuffer + offset)));
      this->second_color_duration.nsec |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->second_color_duration.nsec |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      this->second_color_duration.nsec |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      offset += sizeof(this->second_color_duration.nsec);
     return offset;
    }

    const char * getType(){ return "pal_device_msgs/LedBlinkParams"; };
    const char * getMD5(){ return "0b26c0fa483854105ff61f62de102ca5"; };

  };

}
#endif