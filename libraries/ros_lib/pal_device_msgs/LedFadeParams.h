#ifndef _ROS_pal_device_msgs_LedFadeParams_h
#define _ROS_pal_device_msgs_LedFadeParams_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "std_msgs/ColorRGBA.h"
#include "ros/duration.h"

namespace pal_device_msgs
{

  class LedFadeParams : public ros::Msg
  {
    public:
      typedef std_msgs::ColorRGBA _first_color_type;
      _first_color_type first_color;
      typedef std_msgs::ColorRGBA _second_color_type;
      _second_color_type second_color;
      typedef ros::Duration _transition_duration_type;
      _transition_duration_type transition_duration;
      typedef bool _reverse_fade_type;
      _reverse_fade_type reverse_fade;

    LedFadeParams():
      first_color(),
      second_color(),
      transition_duration(),
      reverse_fade(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->first_color.serialize(outbuffer + offset);
      offset += this->second_color.serialize(outbuffer + offset);
      *(outbuffer + offset + 0) = (this->transition_duration.sec >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->transition_duration.sec >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->transition_duration.sec >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->transition_duration.sec >> (8 * 3)) & 0xFF;
      offset += sizeof(this->transition_duration.sec);
      *(outbuffer + offset + 0) = (this->transition_duration.nsec >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->transition_duration.nsec >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->transition_duration.nsec >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->transition_duration.nsec >> (8 * 3)) & 0xFF;
      offset += sizeof(this->transition_duration.nsec);
      union {
        bool real;
        uint8_t base;
      } u_reverse_fade;
      u_reverse_fade.real = this->reverse_fade;
      *(outbuffer + offset + 0) = (u_reverse_fade.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->reverse_fade);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->first_color.deserialize(inbuffer + offset);
      offset += this->second_color.deserialize(inbuffer + offset);
      this->transition_duration.sec =  ((uint32_t) (*(inbuffer + offset)));
      this->transition_duration.sec |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->transition_duration.sec |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      this->transition_duration.sec |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      offset += sizeof(this->transition_duration.sec);
      this->transition_duration.nsec =  ((uint32_t) (*(inbuffer + offset)));
      this->transition_duration.nsec |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->transition_duration.nsec |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      this->transition_duration.nsec |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      offset += sizeof(this->transition_duration.nsec);
      union {
        bool real;
        uint8_t base;
      } u_reverse_fade;
      u_reverse_fade.base = 0;
      u_reverse_fade.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->reverse_fade = u_reverse_fade.real;
      offset += sizeof(this->reverse_fade);
     return offset;
    }

    const char * getType(){ return "pal_device_msgs/LedFadeParams"; };
    const char * getMD5(){ return "21c3c0f93882d55f24f14be673a6ceb6"; };

  };

}
#endif