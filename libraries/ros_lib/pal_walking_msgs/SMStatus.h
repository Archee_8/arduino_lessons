#ifndef _ROS_pal_walking_msgs_SMStatus_h
#define _ROS_pal_walking_msgs_SMStatus_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "geometry_msgs/Pose.h"

namespace pal_walking_msgs
{

  class SMStatus : public ros::Msg
  {
    public:
      typedef geometry_msgs::Pose _left_foot_pose_type;
      _left_foot_pose_type left_foot_pose;
      typedef geometry_msgs::Pose _right_foot_pose_type;
      _right_foot_pose_type right_foot_pose;
      typedef uint8_t _stance_foot_side_type;
      _stance_foot_side_type stance_foot_side;
      typedef geometry_msgs::Pose _future_swing_landing_pose_type;
      _future_swing_landing_pose_type future_swing_landing_pose;
      typedef uint8_t _future_swing_side_type;
      _future_swing_side_type future_swing_side;
      enum { right = 1                };
      enum { left = 0                 };

    SMStatus():
      left_foot_pose(),
      right_foot_pose(),
      stance_foot_side(0),
      future_swing_landing_pose(),
      future_swing_side(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->left_foot_pose.serialize(outbuffer + offset);
      offset += this->right_foot_pose.serialize(outbuffer + offset);
      *(outbuffer + offset + 0) = (this->stance_foot_side >> (8 * 0)) & 0xFF;
      offset += sizeof(this->stance_foot_side);
      offset += this->future_swing_landing_pose.serialize(outbuffer + offset);
      *(outbuffer + offset + 0) = (this->future_swing_side >> (8 * 0)) & 0xFF;
      offset += sizeof(this->future_swing_side);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->left_foot_pose.deserialize(inbuffer + offset);
      offset += this->right_foot_pose.deserialize(inbuffer + offset);
      this->stance_foot_side =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->stance_foot_side);
      offset += this->future_swing_landing_pose.deserialize(inbuffer + offset);
      this->future_swing_side =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->future_swing_side);
     return offset;
    }

    const char * getType(){ return "pal_walking_msgs/SMStatus"; };
    const char * getMD5(){ return "c7b8f29539d1ef0f23de0a00992a911c"; };

  };

}
#endif