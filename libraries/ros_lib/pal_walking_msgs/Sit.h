#ifndef _ROS_SERVICE_Sit_h
#define _ROS_SERVICE_Sit_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace pal_walking_msgs
{

static const char SIT[] = "pal_walking_msgs/Sit";

  class SitRequest : public ros::Msg
  {
    public:
      typedef bool _down_type;
      _down_type down;
      typedef float _weight_threshold_type;
      _weight_threshold_type weight_threshold;

    SitRequest():
      down(0),
      weight_threshold(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_down;
      u_down.real = this->down;
      *(outbuffer + offset + 0) = (u_down.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->down);
      offset += serializeAvrFloat64(outbuffer + offset, this->weight_threshold);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_down;
      u_down.base = 0;
      u_down.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->down = u_down.real;
      offset += sizeof(this->down);
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->weight_threshold));
     return offset;
    }

    const char * getType(){ return SIT; };
    const char * getMD5(){ return "ad6dfe9fa0ab9703a36ca371cf15816f"; };

  };

  class SitResponse : public ros::Msg
  {
    public:

    SitResponse()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
     return offset;
    }

    const char * getType(){ return SIT; };
    const char * getMD5(){ return "d41d8cd98f00b204e9800998ecf8427e"; };

  };

  class Sit {
    public:
    typedef SitRequest Request;
    typedef SitResponse Response;
  };

}
#endif
