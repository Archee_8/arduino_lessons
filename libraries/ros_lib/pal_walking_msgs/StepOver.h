#ifndef _ROS_SERVICE_StepOver_h
#define _ROS_SERVICE_StepOver_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace pal_walking_msgs
{

static const char STEPOVER[] = "pal_walking_msgs/StepOver";

  class StepOverRequest : public ros::Msg
  {
    public:
      typedef float _step_length_type;
      _step_length_type step_length;
      typedef float _step_time_type;
      _step_time_type step_time;
      typedef float _step_height_type;
      _step_height_type step_height;
      typedef float _z_lipm_type;
      _z_lipm_type z_lipm;

    StepOverRequest():
      step_length(0),
      step_time(0),
      step_height(0),
      z_lipm(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += serializeAvrFloat64(outbuffer + offset, this->step_length);
      offset += serializeAvrFloat64(outbuffer + offset, this->step_time);
      offset += serializeAvrFloat64(outbuffer + offset, this->step_height);
      offset += serializeAvrFloat64(outbuffer + offset, this->z_lipm);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->step_length));
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->step_time));
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->step_height));
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->z_lipm));
     return offset;
    }

    const char * getType(){ return STEPOVER; };
    const char * getMD5(){ return "55de90020d2cc2af87188eee86853f04"; };

  };

  class StepOverResponse : public ros::Msg
  {
    public:
      typedef const char* _result_type;
      _result_type result;

    StepOverResponse():
      result("")
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      uint32_t length_result = strlen(this->result);
      varToArr(outbuffer + offset, length_result);
      offset += 4;
      memcpy(outbuffer + offset, this->result, length_result);
      offset += length_result;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      uint32_t length_result;
      arrToVar(length_result, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_result; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_result-1]=0;
      this->result = (char *)(inbuffer + offset-1);
      offset += length_result;
     return offset;
    }

    const char * getType(){ return STEPOVER; };
    const char * getMD5(){ return "c22f2a1ed8654a0b365f1bb3f7ff2c0f"; };

  };

  class StepOver {
    public:
    typedef StepOverRequest Request;
    typedef StepOverResponse Response;
  };

}
#endif
