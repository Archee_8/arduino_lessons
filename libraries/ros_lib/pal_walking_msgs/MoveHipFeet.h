#ifndef _ROS_SERVICE_MoveHipFeet_h
#define _ROS_SERVICE_MoveHipFeet_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "geometry_msgs/PoseArray.h"
#include "ros/duration.h"

namespace pal_walking_msgs
{

static const char MOVEHIPFEET[] = "pal_walking_msgs/MoveHipFeet";

  class MoveHipFeetRequest : public ros::Msg
  {
    public:
      typedef geometry_msgs::PoseArray _hip_poses_type;
      _hip_poses_type hip_poses;
      typedef geometry_msgs::PoseArray _left_foot_poses_type;
      _left_foot_poses_type left_foot_poses;
      typedef geometry_msgs::PoseArray _right_foot_poses_type;
      _right_foot_poses_type right_foot_poses;
      uint32_t times_after_previous_length;
      typedef ros::Duration _times_after_previous_type;
      _times_after_previous_type st_times_after_previous;
      _times_after_previous_type * times_after_previous;

    MoveHipFeetRequest():
      hip_poses(),
      left_foot_poses(),
      right_foot_poses(),
      times_after_previous_length(0), times_after_previous(NULL)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->hip_poses.serialize(outbuffer + offset);
      offset += this->left_foot_poses.serialize(outbuffer + offset);
      offset += this->right_foot_poses.serialize(outbuffer + offset);
      *(outbuffer + offset + 0) = (this->times_after_previous_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->times_after_previous_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->times_after_previous_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->times_after_previous_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->times_after_previous_length);
      for( uint32_t i = 0; i < times_after_previous_length; i++){
      *(outbuffer + offset + 0) = (this->times_after_previous[i].sec >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->times_after_previous[i].sec >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->times_after_previous[i].sec >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->times_after_previous[i].sec >> (8 * 3)) & 0xFF;
      offset += sizeof(this->times_after_previous[i].sec);
      *(outbuffer + offset + 0) = (this->times_after_previous[i].nsec >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->times_after_previous[i].nsec >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->times_after_previous[i].nsec >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->times_after_previous[i].nsec >> (8 * 3)) & 0xFF;
      offset += sizeof(this->times_after_previous[i].nsec);
      }
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->hip_poses.deserialize(inbuffer + offset);
      offset += this->left_foot_poses.deserialize(inbuffer + offset);
      offset += this->right_foot_poses.deserialize(inbuffer + offset);
      uint32_t times_after_previous_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      times_after_previous_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      times_after_previous_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      times_after_previous_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->times_after_previous_length);
      if(times_after_previous_lengthT > times_after_previous_length)
        this->times_after_previous = (ros::Duration*)realloc(this->times_after_previous, times_after_previous_lengthT * sizeof(ros::Duration));
      times_after_previous_length = times_after_previous_lengthT;
      for( uint32_t i = 0; i < times_after_previous_length; i++){
      this->st_times_after_previous.sec =  ((uint32_t) (*(inbuffer + offset)));
      this->st_times_after_previous.sec |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->st_times_after_previous.sec |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      this->st_times_after_previous.sec |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      offset += sizeof(this->st_times_after_previous.sec);
      this->st_times_after_previous.nsec =  ((uint32_t) (*(inbuffer + offset)));
      this->st_times_after_previous.nsec |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->st_times_after_previous.nsec |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      this->st_times_after_previous.nsec |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      offset += sizeof(this->st_times_after_previous.nsec);
        memcpy( &(this->times_after_previous[i]), &(this->st_times_after_previous), sizeof(ros::Duration));
      }
     return offset;
    }

    const char * getType(){ return MOVEHIPFEET; };
    const char * getMD5(){ return "2ee43076f2da7aab2dbbf598a7709593"; };

  };

  class MoveHipFeetResponse : public ros::Msg
  {
    public:

    MoveHipFeetResponse()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
     return offset;
    }

    const char * getType(){ return MOVEHIPFEET; };
    const char * getMD5(){ return "d41d8cd98f00b204e9800998ecf8427e"; };

  };

  class MoveHipFeet {
    public:
    typedef MoveHipFeetRequest Request;
    typedef MoveHipFeetResponse Response;
  };

}
#endif
