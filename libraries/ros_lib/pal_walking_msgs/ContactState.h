#ifndef _ROS_pal_walking_msgs_ContactState_h
#define _ROS_pal_walking_msgs_ContactState_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace pal_walking_msgs
{

  class ContactState : public ros::Msg
  {
    public:
      typedef uint8_t _type_type;
      _type_type type;
      enum { TYPE_DS = 0 };
      enum { TYPE_LSS = 1 };
      enum { TYPE_RSS = 2 };

    ContactState():
      type(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      *(outbuffer + offset + 0) = (this->type >> (8 * 0)) & 0xFF;
      offset += sizeof(this->type);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      this->type =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->type);
     return offset;
    }

    const char * getType(){ return "pal_walking_msgs/ContactState"; };
    const char * getMD5(){ return "62c8194a0947d63a7094b91a2e31813f"; };

  };

}
#endif