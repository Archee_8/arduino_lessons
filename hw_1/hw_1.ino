void setup() {
  pinMode(13, OUTPUT);
  int i = 0;
  for (int i = 0; i < 10; i++) {//светодиод мигает 10 раз с частотой 2гц
    digitalWrite(13, HIGH);
    delay(250);
    digitalWrite(13, LOW);
    delay(250);

  }
  delay(1000);//подождем 1с
  sos();//запуск SOS

}

void loop() {
}
void point_sos() {
  int point = 300;

  digitalWrite(13, HIGH);
  delay(point);
  digitalWrite(13, LOW);
  delay(point);
}
void dash_sos() {
  int dash = 600;

  digitalWrite(13, HIGH);
  delay(dash);
  digitalWrite(13, LOW);
  delay(dash);
}
void sos() {
  point_sos();
  point_sos();
  point_sos();
  dash_sos();
  point_sos();
  point_sos();
  point_sos();
}
