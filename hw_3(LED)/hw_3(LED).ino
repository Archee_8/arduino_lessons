

#include <ros.h>
#include <std_msgs/Char.h>
#define redLed 13
#define greenLed 12
#define blueLed 11
bool r_l = true;
bool g_l = true;
bool b_l = true;

ros::NodeHandle  nh;

void messageCb( const std_msgs::Char& msg) {
  char c = msg.data;
  switch (c) {
    case 'R':
      if (r_l) {
        r_l = false;
        digitalWrite(redLed, HIGH);
      } else if (!r_l) {
        r_l = true;
        digitalWrite(redLed, LOW);
      }
      break;
      case 'G':
      if (g_l) {
        g_l = false;
        digitalWrite(greenLed, HIGH);
      } else if (!g_l) {
        g_l = true;
        digitalWrite(greenLed, LOW);
      }
      break;
      case 'B':
      if (b_l) {
        b_l = false;
        digitalWrite(blueLed, HIGH);
      } else if (!b_l) {
        b_l = true;
        digitalWrite(blueLed, LOW);
      }
      break;
      default:
      break;
  }
}

ros::Subscriber<std_msgs::Char> sub("Key", &messageCb );

void setup()
{
  pinMode(redLed, OUTPUT);
  pinMode(greenLed, OUTPUT);
  pinMode(blueLed, OUTPUT);
  nh.initNode();
  nh.subscribe(sub);
}

void loop()
{
  nh.spinOnce();
  delay(1);
}
