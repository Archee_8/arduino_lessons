#define buttonPin 2
#define ledPin 9
volatile boolean flag;
void setup() {
  Serial.begin(9600);
  pinMode(buttonPin, INPUT_PULLUP);
  pinMode(ledPin, OUTPUT);
  attachInterrupt(0, buttonTick, FALLING);
}
void buttonTick() {
  flag = true;
}
void loop() {
  if (flag) {
    flag = false;
    (digitalRead(ledPin) == LOW)?digitalWrite(ledPin, HIGH):digitalWrite(ledPin, LOW);
    Serial.println(digitalRead(ledPin));
  }  
}
