#define pinX    A0 
#define ledX     9

void setup() {
  pinMode(ledX, OUTPUT);
  pinMode(pinX, INPUT);
}
 
void loop() {
  int X = analogRead(pinX);      
  X = map(X, 0, 1023, 0, 255);
  analogWrite(ledX, X);
}
