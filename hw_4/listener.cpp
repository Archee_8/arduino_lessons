#include "ros/ros.h"
#include "std_msgs/Float32.h"
#include "std_msgs/String.h"
#include <rosbag/bag.h>
#include <ros/message_event.h>
rosbag::Bag bag("data.bag", rosbag::bagmode::Write);

void water_cb(const std_msgs::Float32::ConstPtr &msg)
{
   
    bag.write("water",ros::Time::now(), *msg);
    ROS_INFO("Water data: %f", msg->data); 

}
void sound_cb(const std_msgs::Float32::ConstPtr &msg)
{
    bag.write("sound",ros::Time::now(), *msg); 
    ROS_INFO("Sound data: %f", msg->data);
}

int main(int argc, char **argv)
{

    ros::init(argc, argv, "listener123");

    ros::NodeHandle n;

    ros::Subscriber water_sub = n.subscribe("water", 1000, water_cb);
    ros::Subscriber sound_sub = n.subscribe("sound", 1000, sound_cb);

    ros::spin();
    bag.close();

    return 0;
}