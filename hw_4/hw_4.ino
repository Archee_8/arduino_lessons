const int sound_pin = A1; // Цифровой вход пин 2
const int water_pin = A0; // Аналоговый вход пин A0

#include <ros.h>
#include <std_msgs/Float32.h>

ros::NodeHandle nh;

std_msgs::Float32 water_msg;
std_msgs::Float32 sound_msg;
ros::Publisher water("water", &water_msg);
ros::Publisher sound("sound", &sound_msg);



void setup()
{
  nh.initNode();
  nh.advertise(sound);
  nh.advertise(water);
  
}

void loop()
{
  sound_msg.data = analogRead(sound_pin);
  sound.publish( &sound_msg );
  water_msg.data = analogRead(water_pin);
  water.publish( &water_msg );
  nh.spinOnce();
  delay(50);
}
