
void setup() {
  pinMode(2, INPUT_PULLUP);
  pinMode(9, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  boolean but = !digitalRead(2);
  if (but) {
    digitalWrite(9, HIGH);
  } else {
    digitalWrite(9, LOW);
  }
}
