const int water_pin = A0; // Аналоговый вход пин A0

#include <ros.h>
#include <std_msgs/Float32.h>
//avg filter
#define NUM_READINGS 500
//median filter
#define STOPPER 0                                     
#define    MEDIAN_FILTER_SIZE    (13)
//kalman filter
float errmeasure = 40; // разброс измерения
float errestimate = 40;  // разброс оценки
float q = 1;  // скорость изменения значений
float currentestimate = 0.0;
float lastestimate = 0.0;
float kalmangain = 0.0;
ros::NodeHandle nh;

std_msgs::Float32 msg;//данные датчика
std_msgs::Float32 f_msg;//фильтрованные данные

ros::Publisher msg_pub("msg", &msg);
ros::Publisher f_msg_pub("f_msg", &f_msg);
//float water;
int average;


void setup()
{
  //  Serial.begin(9600);
  nh.initNode();
  nh.advertise(msg_pub);
  nh.advertise(f_msg_pub);

}

void loop()
{
  msg.data = analogRead(water_pin);
  msg_pub.publish(&msg);
  //Среднее арифметическое
  //  f_msg.data = avg_filter(msg.data);
  //Медианный фильтр
  //  f_msg.data = median_filter(msg.data);
  //Фильтр калмана
    f_msg.data = kalman_filter(msg.data);

  f_msg_pub.publish(&f_msg);
  nh.spinOnce();
}

float avg_filter(float data) {
  long sum = 0;
  for (int i = 0; i < NUM_READINGS; i++) {
    sum += data;
  }
  return sum / NUM_READINGS;
}

float median_filter(float datum)
{
  struct pair
  {
    struct pair   *point;                              /* Pointers forming list linked in sorted order */
    float  value;                                   /* Values to sort */
  };
  static struct pair buffer[MEDIAN_FILTER_SIZE] = {0}; /* Buffer of nwidth pairs */
  static struct pair *datpoint = buffer;               /* Pointer into circular buffer of data */
  static struct pair small = {NULL, STOPPER};          /* Chain stopper */
  static struct pair big = {&small, 0};                /* Pointer to head (largest) of linked list.*/

  struct pair *successor;                              /* Pointer to successor of replaced data item */
  struct pair *scan;                                   /* Pointer used to scan down the sorted list */
  struct pair *scanold;                                /* Previous value of scan */
  struct pair *median;                                 /* Pointer to median */
  float i;

  if (datum == STOPPER)
  {
    datum = STOPPER + 1;                             /* No stoppers allowed. */
  }

  if ( (++datpoint - buffer) >= MEDIAN_FILTER_SIZE)
  {
    datpoint = buffer;                               /* Increment and wrap data in pointer.*/
  }

  datpoint->value = datum;                           /* Copy in new datum */
  successor = datpoint->point;                       /* Save pointer to old value's successor */
  median = &big;                                     /* Median initially to first in chain */
  scanold = NULL;                                    /* Scanold initially null. */
  scan = &big;                                       /* Points to pointer to first (largest) datum in chain */

  /* Handle chain-out of first item in chain as special case */
  if (scan->point == datpoint)
  {
    scan->point = successor;
  }
  scanold = scan;                                     /* Save this pointer and   */
  scan = scan->point ;                                /* step down chain */

  /* Loop through the chain, normal loop exit via break. */
  for (i = 0 ; i < MEDIAN_FILTER_SIZE; ++i)
  {
    /* Handle odd-numbered item in chain  */
    if (scan->point == datpoint)
    {
      scan->point = successor;                      /* Chain out the old datum.*/
    }

    if (scan->value < datum)                        /* If datum is larger than scanned value,*/
    {
      datpoint->point = scanold->point;             /* Chain it in here.  */
      scanold->point = datpoint;                    /* Mark it chained in. */
      datum = STOPPER;
    };

    /* Step median pointer down chain after doing odd-numbered element */
    median = median->point;                       /* Step median pointer.  */
    if (scan == &small)
    {
      break;                                      /* Break at end of chain  */
    }
    scanold = scan;                               /* Save this pointer and   */
    scan = scan->point;                           /* step down chain */

    /* Handle even-numbered item in chain.  */
    if (scan->point == datpoint)
    {
      scan->point = successor;
    }

    if (scan->value < datum)
    {
      datpoint->point = scanold->point;
      scanold->point = datpoint;
      datum = STOPPER;
    }

    if (scan == &small)
    {
      break;
    }

    scanold = scan;
    scan = scan->point;
  }
  return median->value;
}
float kalman_filter(float value) {
  kalmangain = errestimate / (errestimate + errmeasure);
  currentestimate = lastestimate + kalmangain * (value - lastestimate);
  errestimate =  (1.0 - kalmangain) * errestimate + fabs(lastestimate - currentestimate) * q;
  lastestimate = currentestimate;
  return currentestimate;
}
